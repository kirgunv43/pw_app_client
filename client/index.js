import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import routes from '../common/routes'
import { Provider } from 'react-redux'
import { Router, Route, browserHistory } from 'react-router';
import configureStore from '../common/store/configureStore'
import { syncHistoryWithStore } from 'react-router-redux'
import '../common/style/index.scss';

const preloadedState = document.window && window.__PRELOADED_STATE__;


const store = configureStore(preloadedState, window.devToolsExtension && window.devToolsExtension());
const history = syncHistoryWithStore(browserHistory, store);
const rootElement = document.getElementById('root')

render(
  <Provider store={store}>
    <Router history={history}>
       {routes}
    </Router>
  </Provider>, rootElement)
