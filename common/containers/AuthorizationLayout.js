import React, { Component } from 'react';
import Header from '../components/Header';
class AuthorizationLayout extends Component {
  render() {
    return (
      <div>
        <Header {...this.props}/>
        <div className='container content'>
          {this.props.children}
        </div>

      </div>
    )
  }
}

export default AuthorizationLayout;
