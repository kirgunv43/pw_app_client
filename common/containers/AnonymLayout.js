import React, { Component } from 'react';


class AnonymLayout extends Component {
  render() {
    return (
      <div className="container">
        {this.props.children}
      </div>
    )
  }
}

export default AnonymLayout;
