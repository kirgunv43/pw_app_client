import React from 'react';

const InputWrapper = ({reduxFormData, ...rest}) => do {
  <input {...rest} value={reduxFormData.value} onChange={reduxFormData.onChange}/>
}

export default InputWrapper;
