import React, { Component, PropTypes } from 'react';
import cx from 'classnames';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Autosuggest from 'react-autosuggest';
import { changeName, updateSuggestNameAsync, selectUser } from '../../actions/UserSuggest';

class NameSuggest extends Component {

  static propTypes = {

  }

  static defaultProps ={

  }

  getSuggestionValue = (suggestion) => {
    return  suggestion.Name;
  }
  //
  // onChange = (event, { newValue }) => {
  //   this.props.changeName(newValue);
  // }

  renderSuggestion = (suggestion) => {
    return (
      <a href="#" className="list-group-item">{suggestion.Name}</a>
    );
  }

  onSuggestionsUpdateRequested = ({ value }) => {
    this.props.updateSuggestNameAsync(value);
  }

  onSuggestionSelected = (event, { suggestion, suggestionValue, sectionIndex, method }) => {
    this.props.selectUser(suggestion);
    this.props.inputProps.onChange(suggestion)
  }

  render() {
    const { suggestions, inputProps } = this.props;
    return (
      <div>
        <Autosuggest
               onSuggestionSelected={this.onSuggestionSelected}
               onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
               suggestions={suggestions}
               getSuggestionValue={this.getSuggestionValue}
               renderSuggestion={this.renderSuggestion}
               inputProps={{ value: inputProps.value.Name || inputProps.value, onChange: inputProps.onChange, className: this.props.inputClass }}/>
      </div>);
  }
}

function mapStateToProps(state) {
  const { suggestions, name } = state.userSuggest;
  return {suggestions, name};
}

function mapDispatchToProps(dispatch) {
  return { changeName: bindActionCreators(changeName, dispatch),
  updateSuggestNameAsync: bindActionCreators(updateSuggestNameAsync, dispatch),
  selectUser: bindActionCreators(selectUser, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(NameSuggest)
