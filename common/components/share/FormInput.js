import React, { Component, PropTypes } from 'react';
import InputWrapper from './InputWrapper';
import cx from 'classnames';

class FormInput extends Component {

  static propTypes = {
    reduxFormData: PropTypes.object,
    label: PropTypes.string
  }

  render() {
    const { reduxFormData } = this.props;
    const { label, ...rest } = this.props;
    
    const fg_classes = cx('form-group', { 'has-danger': reduxFormData.error });
    const input_classes = cx(this.props.className, { 'form-control-danger': reduxFormData.error });

    return (
      <div className={fg_classes}>
        <label>{label}</label>
        <InputWrapper {...rest} className={input_classes}/>
        {reduxFormData.error ? (<div className="form-control-feedback">{reduxFormData.error}</div>) : null}
      </div>);
  }
}

export default FormInput;
