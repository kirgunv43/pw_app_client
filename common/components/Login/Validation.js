import fetch from 'isomorphic-fetch';
import { requestToken } from '../../api/Login';
import { setToken } from '../../actions/Login';
import { push } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { updateBalance } from '../../api/CurrentUser';
import { setBalance } from '../../actions/CurrentUser';

const validation = (values, dispatch) => {
  return new Promise(async (resolve, reject) => {
    let errors = {};
    if (!values.password) {
      errors.password = 'Required'
    } else if (values.password.length < 6) {
      errors.password = 'Must be 6 characters or more'
    }

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (Object.keys(errors).length) {
      reject(errors);
      return;
    }

    const request_token = await requestToken(values);
    const json = await request_token.json();
    console.log(json);
    if (request_token.status === 200) {
      dispatch(setToken(json.access_token, json['.expires']));
      dispatch(push('/App/Operations'));
      resolve();
    } else {
      reject({common: json.error_description});
    }
  });
};


export default validation;
