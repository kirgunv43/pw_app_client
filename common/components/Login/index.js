import React, { Component, PropTypes } from 'react';
import {reduxForm} from 'redux-form';
import FormInput from '../share/FormInput';
import InputWrapper from '../share/InputWrapper';
import Validation from './Validation';
import { Link } from 'react-router'

class Login extends Component {

  getCommonError() {
    const { fields: { common } } = this.props;
    if (!common.error) {
      return null;
    }
    return (
      <div className="alert alert-danger" role="alert">
        {common.error}
      </div>
    )
  }

  render() {
    const { fields: { email, password, remember }, handleSubmit } = this.props;

    return (
      <div>
        <form className="form-signin" onSubmit={handleSubmit(Validation)}>
          <h2 className="form-signin-heading">Please sign in</h2>
          {this.getCommonError()}
          <FormInput
            label='Email address'
            className="form-control"
            type="text"
            name='email'
            placeholder="Type email address"
            reduxFormData={email}
            noValidate/>
          <FormInput
            label='Password'
            className="form-control"
            type="password"
            placeholder="Type password"
            reduxFormData={password}
            noValidate/>
          <div className="checkbox">
            <label>
              <InputWrapper type="checkbox" value="remember-me" reduxFormData={remember}/> Remember me
            </label>
          </div>
          <button type="submit" className="btn btn-success btn-block">Sign in</button>
          <Link to='/Authorization/Register' className="btn btn-primary btn-block">Sign up</Link>
        </form>
      </div>
      )
    }
}

const LoginForm = reduxForm({
  form: 'login',
  fields: ['email', 'password', 'remember', 'common']
})(Login);
export default LoginForm;
