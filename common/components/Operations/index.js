import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux'
import NameSuggest from '../share/NameSuggest'
import { bindActionCreators } from 'redux'
import {reduxForm} from 'redux-form';
import cx from 'classnames';
import Validation from './Validation';
class Operations extends Component{


  handleAddressSave = () => {
    this.props.handleSubmit(
      (values, dispatch) => validateAddress(values, dispatch, this.props.balance)
    )();
  }

  render() {
    const { fields: { user, pw }, handleSubmit } = this.props;

    const suggest_classes = cx('form-group row', { 'has-danger': user.error });
    const pw_classes = cx('form-group row', { 'has-danger': pw.error });

    const pw_input_classes = cx('form-control', { 'form-control-danger': pw.error });
    const suggest_input_classes = cx('form-control', { 'form-control-danger': user.error });

    const { value, onChange } = pw;
    return (
      <div>
        <form onSubmit={handleSubmit(Validation)}>
          <div className={suggest_classes}>
            <label htmlFor="example-text-input" className="col-xs-2 col-form-label">Name</label>
            <div className="col-xs-8">
              <NameSuggest inputProps={user} inputClass={suggest_input_classes}/>
              <span className="form-control-feedback">{user.error}</span>
            </div>

          </div>
          <div className={pw_classes}>
            <label htmlFor="example-text-input" className="col-xs-2 col-form-label">PW</label>
            <div className="col-xs-8">
              <input {...{ value, onChange }} className={pw_input_classes}/>
                <span className="form-control-feedback">{pw.error}</span>
            </div>
          </div>
          <p><button type="button" onClick={this.handleAddressSave} className="btn btn-lg btn-primary">Create</button></p>

        </form>
      </div>
    )
  }
}


const OperationsForm = reduxForm({
  form: 'operations',
  fields: ['user', 'pw']
})(Operations);

function mapStateToProps(state) {
  const { balance } = state.currentUser;
  return {balance};
}



export default connect(mapStateToProps)(OperationsForm)
