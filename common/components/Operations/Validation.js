import fetch from 'isomorphic-fetch';
import { requestToken } from '../../api/Login';
import { setToken } from '../../actions/Login';
import { push } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { updateBalance } from '../../api/CurrentUser';
import { setBalance } from '../../actions/CurrentUser';

const validation = (values, dispatch, balance) => {
  return new Promise(async (resolve, reject) => {
    let errors = {};

    if (!values.user || (values.user && !values.user.Name)) {
      errors.user = 'Required'
    }

    if (!values.pw) {
      errors.pw = 'Required'
    } else if (Number(balance) < Number(pw)) {
      errors.pw = '111'
    }

    if (Object.keys(errors).length) {
      reject(errors);
      return;
    }

    const request_token = await requestToken(values);
    const json = await request_token.json();

    if (request_token.status === 200) {
      dispatch(setToken(json.access_token, json['.expires']));
      dispatch(push('/App/Operations'));
      resolve();
    } else {
      reject({common: json.error_description});
    }
  });
};


export default validation;
