import React, { Component, PropTypes } from 'react';
import { loadBalance } from '../actions/CurrentUser';
import { setBalance } from '../actions/CurrentUser';
import { signOut } from '../api/Login';
import { connect } from 'react-redux'
import { Link } from 'react-router'
class Header extends Component{
  constructor(props, con) {
    super(props);
    console.log(con);
  }

  logout = () => {
    signOut();
  }

  componentWillMount() {
    this.props.dispatch(loadBalance());
  }

  render() {
    const operationsIsActive = this.props.location.pathname.search('/App/Operations') >= 0 ? ' active' : '';
    const historyIsActive = this.props.location.pathname.search('/App/History') >= 0 ? ' active' : '';
    return (
      <nav className="navbar navbar-fixed-top navbar-dark bg-inverse">
        <Link className="navbar-brand" activeClassName='active' to='/App/Operations'>Test name</Link>
        <ul className="nav navbar-nav">
          <li className="nav-item">
            <Link className={"nav-link" + operationsIsActive} to='/App/Operations'>Operations</Link>
          </li>
          <li className="nav-item">
            <Link className={"nav-link" + historyIsActive}  to='/App/History' >History</Link>
          </li>

        </ul>

        <form className="form-inline pull-xs-right">
            <span className='balance nav-item'>Balance: <span>{this.props.balance}</span> PW</span>
            <button onClick={this.logout} className="btn btn-outline-success" type="submit">Logout</button>
        </form>
      </nav>
    )
  }
}

function mapStateToProps(state) {
  const { balance } = state.currentUser;
  return {balance};
}

function mapDispatchToProps(dispatch) {
  return { dispatch: dispatch }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)
