import fetch from 'isomorphic-fetch';
import { register } from '../../api/Register';
import { requestToken } from '../../api/Login';
import { setToken } from '../../actions/Login';
import { push } from 'react-router-redux';
import { updateBalance } from '../../api/CurrentUser';
import { setBalance } from '../../actions/CurrentUser';

const validation = (values, dispatch) => {
  return new Promise(async (resolve, reject) => {
    let errors = {};

    if (!values.name) {
      errors.name = 'Required'
    }

    if (!values.email) {
      errors.email = 'Required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }

    if (!values.password) {
      errors.password = 'Required'
    } else if (values.password.length < 6) {
      errors.password = 'Must be 6 characters or more'
    }

    if (values.confirm_password && values.password && values.password !== values.confirm_password) {
      errors.confirm_password = 'Password shall be equal confirm password'
    }

    if (Object.keys(errors).length) {
      reject(errors);
      return;
    }
    const response = await register({
      FIO: values.name,
      Email: values.email,
      Password: values.password,
      ConfirmPassword: values.confirm_password
    });

    if (response.status === 200) {
      const request_token = await requestToken(values);
      const token_data = await request_token.json();
      const action = setToken(token_data.access_token, token_data['.expires']);
      dispatch(action);
      dispatch(push('/App/Operations'));
      resolve();
    } else {
      const json = await response.json();
      reject({common: Object.values(json.ModelState)});
    }
  });
};


export default validation;
