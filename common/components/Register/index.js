import React, { Component, PropTypes } from 'react';
import {reduxForm} from 'redux-form';
import FormInput from '../share/FormInput';
import InputWrapper from '../share/InputWrapper';
import Validation from './Validation';
import { Link } from 'react-router'
class Register extends Component {

  getCommonError() {
    const { fields: { common } } = this.props;
    if (!common.error) {
      return null;
    }
    return (
      <div className="alert alert-danger" role="alert">
        {common.error}
      </div>
    )
  }

  render() {
    const { fields: { email, password, name, confirm_password }, handleSubmit } = this.props;

    return (
      <div>
        <form className="form-signin" onSubmit={handleSubmit(Validation)}>
          <h2 className="form-signin-heading">Please sign in</h2>
          {this.getCommonError()}
          <FormInput
            label='User'
            name='username'
            className="form-control"
            type="text"
            placeholder="Type you name"
            reduxFormData={name}
            noValidate/>
          <FormInput
            label='Email'
            name='email'
            className="form-control"
            type="text"
            placeholder="Type email address"
            reduxFormData={email}
            noValidate/>
          <FormInput
            label='Password'
            className="form-control"
            type="password"
            placeholder="Type password"
            reduxFormData={password}
            noValidate/>
          <FormInput
            label='Confirm password'
            className="form-control"
            type="password"
            placeholder="Repeat password"
            reduxFormData={confirm_password}
            noValidate/>
          <button type="submit" className="btn btn-primary btn-block">Sign up</button>
          <Link to='/Authorization/Login' className="btn btn-success btn-block">Sign in</Link>
        </form>
      </div>
      )
    }
}

const RegisterForm = reduxForm({
  form: 'register',
  fields: ['email', 'password', 'confirm_password', 'name', 'common']
})(Register);
export default RegisterForm;
