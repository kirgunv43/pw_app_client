import { NAME_CHANGE, UPDATE_SUGGEST, SELECT_USER } from '../actions/UserSuggest'

const initState = {
  suggestions: [],
  name: ''
}

function userSuggest (state = initState, action) {
  switch (action.type) {
    case SELECT_USER: {
      return { ...state, name: action.user.Name, selected: action.user.Id };
    }
    case NAME_CHANGE: {
      return { ...state, name: action.name , selected: undefined };
    }
    case UPDATE_SUGGEST: {
      return { ...state, suggestions: action.suggestions };
    }
    default:
      return {...state};
  }
}

export default userSuggest;
