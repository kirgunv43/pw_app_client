import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';
import login from './Login';
import userSuggest from './UserSuggest';
import currentUser from './CurrentUser';

const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  login,
  userSuggest,
  currentUser
})

export default rootReducer
