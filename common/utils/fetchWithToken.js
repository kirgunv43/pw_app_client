import fetch from 'isomorphic-fetch';
import Cookies from 'react-cookie';

const authorization = Cookies.load('Authorization');
const expires = Cookies.load('.expires');


function fetchWithToken(url, data) {
    return fetch(url, {
        ...data,
        headers: {
          "Authorization": Cookies.load('Authorization'),
          "Content-type": 'application/json; charset=utf-8',
          ...( data && data.headers ? data.headers : {})
        }
    });
}

export default fetchWithToken;
