import fetchWithToken from '../utils/fetchWithToken';
import fetch from 'isomorphic-fetch';
export const requestToken = async (data) => {
  return fetch(`/Token`,
  {
    method: 'POST',
    body: `grant_type=password&username=${data.email}&password=${data.password}`,
    headers: {
      'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
      'Content-Type': 'application/json; charset=utf-8'
    }
  });
}

export const signOut = () => {
  return fetchWithToken(`/api/v1/Account/Logout`, { method: 'POST' });
}
