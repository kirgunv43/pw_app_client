import fetch from '../utils/fetchWithToken';

export const updateBalance = async () => {
  const response = await fetch(`/api/v1/Users/balance`);
  const json = await response.json();
  return json;
}
