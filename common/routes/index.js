import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router';
import AnonymLayout from '../containers/AnonymLayout';
import AuthorizationLayout from '../containers/AuthorizationLayout';
import BaseLayout from '../containers/BaseLayout';
import NotFound from '../containers/NotFound';

import Operations from '../components/Operations';
import History from '../components/History';

import Login from '../components/Login';
import Register from '../components/Register';

import Cookies from 'react-cookie';

const userIsAuthorized = (nextState, replace, callback) => {
  const authorization = Cookies.load('Authorization');
  const expires = Cookies.load('.expires');
  if (nextState.location.pathname.search('/Authorization') < 0) {
    console.log(`!authorization: ${authorization}`);
    console.log(`((+ new Date(expires) < Date.now())): ${((+ new Date(expires) < Date.now()))}`);
    if (!authorization || ((+ new Date(expires) < +Date.now()))) {
      console.log('redirect');
      replace(`/Authorization/Login`)
    }
  }

  callback();
}

const routes = (
    <Route path="/" component={BaseLayout}  onEnter={userIsAuthorized}>
      <Route path="/Authorization" component={AnonymLayout}>
        <Route path="Login" component={Login}/>
        <Route path="Register" component={Register}/>
      </Route>
      <Route path="/App" component={AuthorizationLayout}>
        <Route path="Operations" component={Operations}/>
        <Route path="History" component={History}/>
      </Route>
      <Route path="*" component={NotFound}/>
    </Route>);


export default routes;
