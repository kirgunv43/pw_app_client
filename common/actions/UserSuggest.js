import fetch from '../utils/fetchWithToken';
export const NAME_CHANGE = 'NAME_CHANGE';
export const UPDATE_SUGGEST = 'UPDATE_SUGGEST';
export const SELECT_USER = 'SELECT_USER';

export const changeName = (name) => {
   return {
     type: NAME_CHANGE,
     name
   }
}


export const selectUser = (user) => {
   return {
     type: SELECT_USER,
     user
   }
}

export const updateSuggest = (suggestions) => {
   return {
     type: UPDATE_SUGGEST,
     suggestions
   }
}


export const updateSuggestNameAsync = (name) => async (dispatch) => {
   const response = await fetch(`/api/v1/Users/suggest?name=${encodeURI(name)}`);
   if (response.status === 200) {
     const json = await response.json();
     dispatch(updateSuggest(JSON.parse(json)));
   }
}
