import fetch from 'isomorphic-fetch';
const REQUEST_AUTHORIZATION = 'REQUEST_AUTHORIZATION';

export const register = (data) => (dispatch) => {
  fetch('/api/v1/Register',
    {
      method: 'POST',
      body: JSON.stringify(data)
    }
  );
}
