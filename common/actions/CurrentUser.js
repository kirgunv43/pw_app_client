import fetch from '../utils/fetchWithToken';
import {updateBalance} from '../api/CurrentUser';

export const SET_BALANCE = 'SET_BALANCE';

export const setBalance = (balance) => {
  return {type: SET_BALANCE, balance};
}

export const loadBalance = () => async (dispatch) => {
  var balance = await updateBalance();
  dispatch(setBalance(JSON.parse(balance).balance));
}
