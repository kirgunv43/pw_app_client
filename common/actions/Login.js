import fetch from 'isomorphic-fetch';
export const SET_TOKEN = 'SET_TOKEN';
import Cookies from 'react-cookie';

export const setToken = (token, expires) => {
  Cookies.save('Authorization', `Bearer ${token}`, { path: '/' });
  Cookies.save('.expires', expires, { path: '/' });
  return {
    type: SET_TOKEN,
    token
  }
}

/*  fetch('/api/v1/values', {
    headers: {
      "Authorization": Cookies.get('Authorization')
    }
  }).then(resp => resp.json()).then(json => console.log(json))*/
